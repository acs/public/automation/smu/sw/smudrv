#!/bin/sh
trap '' INT

is_std_user () { [ "$(id -u)" -ne 0 ]; }
if is_std_user; then
    echo 'Please run as root.' >&2
    exit 1
fi

# clear kernel messages and screen
dmesg --clear ; clear
echo "Inserting kernel module\n"

#insert kernel module
insmod ../../build/smudrv.ko
if [ $? -ne 0 ]; then
    echo 'Please build the module first.' >&2
    exit 1
fi

# run test
( trap - INT; ./smudrv_test )

# remove kernel module
rmmod smudrv
echo "\nRemoving kernel module\n"

#print kernel messages
echo "Kernel messages\n"
dmesg 
