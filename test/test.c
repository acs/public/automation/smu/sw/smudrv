// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <errno.h>
#include <stdlib.h>

#define NUM_PAGE    800
#define PAGE_SIZE   4096
#define MAX_SIZE    (PAGE_SIZE * NUM_PAGE)

#define SMU_IOC_MAGIC           'k'
#define SMU_IOC_REG_TASK        _IO (SMU_IOC_MAGIC, 0)
#define SMU_IOC_RESET           _IO (SMU_IOC_MAGIC, 1)
#define SMU_IOC_START           _IO (SMU_IOC_MAGIC, 2)
#define SMU_IOC_STOP            _IO (SMU_IOC_MAGIC, 3)
#define SMU_IOC_SET_CONF        _IOW(SMU_IOC_MAGIC, 4, smu_conf_t*)
#define SMU_IOC_GET_CONF        _IOR(SMU_IOC_MAGIC, 5, smu_conf_t*)
#define SMU_IO_MAX_NUM          6

#define SMU_SIG_SYNC            41
#define SMU_SIG_DATA            42

#define DEVICE_FILENAME "/dev/smu"

typedef struct
{
    uint8_t fs;                                                     // Sample rate (kHz)
    uint8_t fb;                                                     // Buffer rate (Hz)
    uint8_t mode;                                                   // Operating mode
    uint8_t sync;                                                   // Synchronization
} __attribute__((__packed__)) smu_conf_t;

FILE *fdata;
char fname[20];
int fd;
char *sh_data = NULL;
short int *u = NULL;
uint8_t Fs = 200;

static void signal_sync_handler(int n, siginfo_t *info, void *unused){
    printf("si: no %d\t err %d\tcode %d\tval %d\n",
            info->si_signo,
            info->si_errno,
            info->si_code,
            info->si_value.sival_int);

}

static void signal_data_handler(int n, siginfo_t *info, void *unused)
{
    printf("si:\tno %d\terr %d\tcode %d\tval %d\n",
            info->si_signo,
            info->si_errno,
            info->si_code,
            info->si_value.sival_int);

}

// Free memory segments and exit
static void signal_terminate(int n, siginfo_t *info, void *unused)
{
    if (ioctl(fd, SMU_IOC_STOP, 0))
        printf("ioctl stop failed");

    u = (short int*)sh_data;
    unsigned int i, j;
    for (i = 0; i < 100; i++){
        for (j = 0; j < 8; j++)
            printf("%d\t", u[i*8+j]);
        printf("\n");
    }

    if (strlen(fname)<5)
        goto out;

    fdata = fopen(fname,"w");
    for (i = 0; i < Fs*1000; i++){
        for (j = 0; j < 8; j++)
            fprintf(fdata, "%d,", u[i*8+j]);
        fseek(fdata,-1,SEEK_CUR);
        fprintf(fdata,"\n");
    }
    fclose(fdata);

out:    
    munmap(sh_data, MAX_SIZE);
    close(fd);
    exit(0);
}

int main(int argc, char *argv[])
{
    int ret = -1;

    if (argc > 1)
        sprintf(fname,"data_%s.csv",argv[1]);

    struct sigaction act;
    sigemptyset(&act.sa_mask);
    act.sa_flags = (SA_SIGINFO | SA_RESTART);
    act.sa_sigaction = signal_sync_handler;
    sigaction(SMU_SIG_SYNC, &act, NULL);

    sigemptyset(&act.sa_mask);
    act.sa_flags = (SA_SIGINFO | SA_RESTART);
    act.sa_sigaction = signal_data_handler;
    sigaction(SMU_SIG_DATA, &act, NULL);

    // Ensure cleanup if user hits ctrl-C
    sigemptyset(&act.sa_mask);
    act.sa_flags = (SA_SIGINFO | SA_RESETHAND);
    act.sa_sigaction = signal_terminate;
    sigaction(SIGINT, &act, NULL);

    fd = open(DEVICE_FILENAME, O_RDWR|O_NDELAY);

    if(fd < 0){
        printf("unable to open driver\n");
        goto r_out;
    }

    /* register this task with kernel for signal */
    if (ioctl(fd, SMU_IOC_REG_TASK,0)){
        printf("ioctl handshake failed\n");
        goto r_fid;
    }

    /* mapping kernel memory */
    sh_data = (char*)mmap(0,
                    MAX_SIZE,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    fd,
                    0);
    if (sh_data == NULL)
        printf("unable to map memory");


    /* send ping signal to kernel*/
    smu_conf_t conf = { Fs, 100, 1, 1};
    if (ioctl(fd, SMU_IOC_SET_CONF, &conf)){
        printf("ioctl set conf failed");
        goto r_fid;
    }
    if (ioctl(fd, SMU_IOC_START, 0)){
        printf("ioctl start failed");
        goto r_fid;
    }
    printf("Waiting for sync signal.. (Ctrl+C to exit)\n");

    while(1)
        sleep(1);
r_fid:
    close(fd);
r_out:
    return ret;
}
