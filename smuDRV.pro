#-----------------------------------------------------------------
#
#   Carlo Guarnieri Calò Carducci, PhD
#	cguarnieri@eonerc.rwth-aachen.de
#
#-----------------------------------------------------------------

QT       = core

CONFIG  += c

SOURCES +=  main.c \
            include/sub_daq.c  \
            include/sub_dev.c  \
            include/sub_rpi.c

HEADERS +=  include/sub_daq.h  \
            include/sub_dev.h  \
            include/sub_rpi.h

DISTFILES +=
