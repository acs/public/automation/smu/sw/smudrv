// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#include <linux/init.h>                                             // Macros used to mark up functions e.g., __init __exit
#include <linux/kernel.h>                                           // Contains types, macros, functions for the kernel
#include <linux/module.h>                                           // Core header for loading LKMs into the kernel

#include <sub_dev.h>                                                // SMU character device interface

/**
 * @brief Driver init
 *
 * Init the SMU character device
 */
static int __init smuDRV_init(void)
{
    printk(KERN_INFO "smudrv: init module\n");
    return dev_init();
}

/**
 * @brief Driver exit
 *
 * Exit the SMU character device
 */
static void __exit smuDRV_exit(void)
{
    dev_exit();
    printk(KERN_INFO "smudrv: exit module\n");
}

module_init(smuDRV_init);
module_exit(smuDRV_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Carlo Guarnieri Calo' Carducci, PhD <carlo.guarnieri@ieee.org>");
MODULE_DESCRIPTION("SMU Driver");
MODULE_VERSION("2.0");
MODULE_ALIAS("platform:raspberrypi-smu");
