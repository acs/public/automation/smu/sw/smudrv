EXTRA_CFLAGS := -I$(src)/include

obj-m += smudrv.o

smudrv-objs := main.o include/sub_daq.o include/sub_dev.o include/sub_rpi.o

all:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) modules -j4 -o3
	cp -f smudrv.ko $(PWD)/../build
clean:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) clean
	rm -f $(PWD)/../build/smudrv.ko
