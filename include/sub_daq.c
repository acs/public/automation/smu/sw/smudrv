// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#include "sub_daq.h"
#include "sub_dev.h"
#include "sub_rpi.h"

smu_conf_t smu_conf = { 10, 100, MODE_FREERUN, SYNC_PPS};
uint32_t buff_pos, buff_step, buff_size, daq_status;

/**
 * @brief Configure PWM
 *
 * Configures the clock source and pwm block
 * for requested sample and buffer rates
 *
 * @return error
 */
static int conf_pwm(void)
{
    uint32_t divi, range_fs, range_fb;

    /// Check rates valid
    if ((FS_MAX % smu_conf.fs > 0) || (FB_MAX % smu_conf.fb > 0)){
        printk(KERN_ERR "smudrv: invalid sample or buffer rate\n");
        return -EINVAL;
    }
    
    divi = 2;                                                       // Clock divisor
    range_fs = CLK_F_PLLD / divi / smu_conf.fs;                     // Sample rate
    range_fb = CLK_F_PLLD / divi / smu_conf.fb * 1000;              // Buffer rate

    /// Reset PWM error status
    if (_reg(pwm_reg,  PWM_STA) & PWM_STA_BERR)
        _reg(pwm_reg,  PWM_STA) = PWM_STA_BERR;

    udelay(1000);

    /// Configure PWM Clock Manager
    _reg(clk_reg, CLK_PWM_CTL) = CLK_PASSWD | CLK_CTL_KILL;         // Kill clock
    udelay(10);
    while ((_reg(clk_reg, CLK_PWM_CTL) & CLK_CTL_BUSY) != 0)        // Wait clock stop
        udelay(10);
    _reg(clk_reg, CLK_PWM_DIV) = CLK_PASSWD |
                                 CLK_DIV_DIVI(divi);                // Set clock divisor
    _reg(clk_reg, CLK_PWM_CTL) = CLK_PASSWD |
                                 CLK_CTL_SRC(CLK_SRC_PLLD);         // PLLD @ 500 MHz
    udelay(10);
    _reg(clk_reg, CLK_PWM_CTL)|= CLK_PASSWD | CLK_CTL_ENAB;         // Start clock
    udelay(10);
    while ((_reg(clk_reg, CLK_PWM_CTL) & CLK_CTL_BUSY) == 0)        // Wait clock start
        udelay(10);
    
    /// Configure PWM mode
    _reg(pwm_reg, PWM_CTL) = PWM_CTL_MSEN1 | PWM_CTL_MSEN2;

    /// Configure PWM frequency
    _reg(pwm_reg, PWM_RNG2) = range_fs;                             // Fs
    _reg(pwm_reg, PWM_DAT2) = range_fs - 200;                       // DC min 84%

    _reg(pwm_reg, PWM_RNG1) = range_fb;                             // Fb
    _reg(pwm_reg, PWM_DAT1) = range_fb - 125000;                    // DC min 90%
    
    /// Check PWM error status
    if (_reg(pwm_reg,  PWM_STA) & PWM_STA_BERR){
        printk(KERN_ERR "smudrv: fail to configure pwm\n");
        return -EBUSY;
    }

    return 0;
}

/**
 * @brief Configure FSM
 *
 * Configure finite state machine for data acquisition
 * using DMA and SMI parallel interface
 *
 * @return error
 */
static int conf_fsm(void) {
    /// Init bus memory
    dma_cb_t *cb = sh_conf;
    uint32_t  dma_blocks = 7;                                       // DMA blocks
    uint32_t *daq_start = (uint32_t *)&cb[dma_blocks];
    uint32_t *daq_stop  = daq_start + 1;
    uint32_t *smi_start = daq_start + 2;
    uint32_t *smi_n_pps = daq_start + 3;
    uint32_t *smi_n_adc = daq_start + 4;

    /// Init virtual memory
    memset(sh_data, 0, MMAP_SIZE);
    memset(sh_conf, 0, PAGE_SIZE);
    
    /// Check modes valid
    if (!(smu_conf.mode < MODE_MAX) || !(smu_conf.sync < SYNC_MAX)){
        printk(KERN_ERR "smudrv: invalid mode or sync source\n");
        return -EINVAL;
    }

    *daq_start = PWM_CTL_MSEN1 | PWM_CTL_PWEN1 | PWM_CTL_MSEN2 | PWM_CTL_PWEN2;
    *daq_stop  = PWM_CTL_MSEN1 | PWM_CTL_MSEN2;
    *smi_start = SMI_CS_ENABLE | SMI_CS_START | SMI_CS_CLEAR | SMI_CS_PXLDAT;
    *smi_n_pps = 1UL;
    *smi_n_adc = smu_conf.fs * 1000UL * SAMPLE_SIZE;
    udelay(100);

    /// Init FSM

    /// Set SMI transaction length to PWM register
    cb[0].ti = DMA_WAIT_RESP;
    cb[0].tfr_len = sizeof(uint32_t);
    cb[0].srce_ad = _v2b(sh_conf, dma_conf, smi_n_pps);
    cb[0].dest_ad = SMI_BUS_BASE + SMI_L;
    cb[0].next_cb = _v2b(sh_conf, dma_conf, &cb[1]);

    /// Start SMI
    cb[1].ti = DMA_WAIT_RESP;
    cb[1].tfr_len = sizeof(uint32_t);
    cb[1].srce_ad = _v2b(sh_conf, dma_conf, smi_start);
    cb[1].dest_ad = SMI_BUS_BASE + SMI_CS;
    cb[1].next_cb = _v2b(sh_conf, dma_conf, &cb[2]);

    /// Start PWM
    if (smu_conf.sync == SYNC_PPS)                                  // PPS sync start
        cb[2].ti = DMA_SRCE_DREQ | DMA_PERMAP(DMA_SMI_DREQ) | DMA_WAIT_RESP;
    else
        cb[2].ti = DMA_WAIT_RESP;                                   // No sync start
    cb[2].tfr_len = sizeof(uint32_t);
    cb[2].srce_ad = _v2b(sh_conf, dma_conf, daq_start);             // Start PWM
    cb[2].dest_ad = PWM_BUS_BASE + PWM_CTL;
    cb[2].next_cb = _v2b(sh_conf, dma_conf, &cb[3]);

    /// Set SMI transaction length
    cb[3].ti = DMA_WAIT_RESP;
    cb[3].tfr_len = sizeof(uint32_t);
    cb[3].srce_ad = _v2b(sh_conf, dma_conf, smi_n_adc);
    cb[3].dest_ad = SMI_BUS_BASE + SMI_L;
    cb[3].next_cb = _v2b(sh_conf, dma_conf, &cb[4]);

    /// Start SMI
    cb[4].ti = DMA_WAIT_RESP;
    cb[4].tfr_len = sizeof(uint32_t);
    cb[4].srce_ad = _v2b(sh_conf, dma_conf, smi_start);
    cb[4].dest_ad = SMI_BUS_BASE + SMI_CS;
    cb[4].next_cb = _v2b(sh_conf, dma_conf, &cb[5]);

    /// Start data transfer
    if (smu_conf.sync == MODE_VIRTUAL){
        cb[5].ti = DMA_SRCE_INC | DMA_DEST_INC | DMA_WAIT_RESP;     // Start immediately
        cb[5].srce_ad = dma_data + VC_0_DIRECT_UNCACHED;            // Data from virtual
    } else {
        cb[5].ti = DMA_SRCE_DREQ | DMA_PERMAP(DMA_SMI_DREQ) |       // Start on DREQ
                   DMA_DEST_INC;
        cb[5].srce_ad = SMI_BUS_BASE + SMI_D;                       // Data from SMI
    }
    cb[5].tfr_len = smu_conf.fs * 1000UL * SAMPLE_SIZE;
    cb[5].dest_ad = dma_data + VC_0_DIRECT_UNCACHED;
    if (smu_conf.mode == MODE_VIRTUAL)
        cb[5].next_cb = _v2b(sh_conf, dma_conf, &cb[3]);            // Start new data transfer
    else
        cb[5].next_cb = _v2b(sh_conf, dma_conf, &cb[6]);            // Stop PWM

    /// Stop PWM
    cb[6].ti = 0;
    cb[6].tfr_len = sizeof(uint32_t);
    cb[6].srce_ad = _v2b(sh_conf, dma_conf, daq_stop);
    cb[6].dest_ad = PWM_BUS_BASE + PWM_CTL;
    if ((smu_conf.mode == MODE_FREERUN) && (smu_conf.sync == SYNC_PPS))
        cb[6].next_cb = _v2b(sh_conf, dma_conf, &cb[0]);            // Start new data transfer with sync
    else
        cb[6].next_cb = 0;                                          // Stop DMA

    return 0;
}

/**
 * @brief Init DAQ
 *
 * Init ADC and required peripherals
 *
 * @return ret, error code
 */
int init_daq(void)
{
	/// ADC reset sequence
    gpio_out(ADC_STDBY, LOW);
    gpio_out(ADC_RESET, LOW);
    gpio_out(ADC_RANGE, LOW);
    mdelay(1);
    gpio_out(ADC_STDBY,HIGH);
    gpio_out(ADC_RANGE,HIGH);                                       // Set range +-10V
    mdelay(20);
    gpio_out(ADC_RESET,HIGH);
    mdelay(100);
    gpio_out(ADC_RESET, LOW);

    /// Set SMI digital input
    gpio_out(MUX_UART, HIGH);

    /// Set AI0 analog input
    gpio_out(MUX_VREF, HIGH);

    daq_status = DAQ_IDLE;
    printk(KERN_INFO "smudrv: daq init complete\n");

    return 0;
}

/**
 * @brief Configure DAQ
 *
 * Configures DAQ with smu_conf parameters
 *
 * @return error
 */
int conf_daq(void)
{
    int ret = 0;

    /// Configure PWM
    ret += conf_pwm();

    /// Configure FSM
    ret += conf_fsm();

    return ret;
}

/**
 * @brief Exit DAQ
 *
 * Free requested resources
 */
void dest_daq(void)
{
    /// Shutdown ADC
    gpio_out(ADC_RANGE, LOW);
    gpio_out(ADC_STDBY, LOW);
}

/**
 * @brief Play DAQ
 *
 * Start DAQ finite state machine
 */
void play_daq(void) {
    buff_pos  = 0;
    buff_size = smu_conf.fs * 1000; // * SAMPLE_SIZE;
    buff_step = buff_size / smu_conf.fb;

    /// Check DAQ configuration
    if (conf_daq() < 0){
        printk(KERN_ERR "smudrv: bad configuration, daq stopped\n");
        return;
    }

    _reg(dma_reg, DMA_ENABLE) |= (1 << DMA_CHAN);                   // Set DMA channel
    _reg(dma_reg, DMA_CS) = DMA_CS_RESET;                           // Reset DMA
    _reg(dma_reg, DMA_CONBLK_AD) = dma_conf;                        // Set first DMA control block
    _reg(dma_reg, DMA_CS) = DMA_CS_END;                             // Clear 'end' flag
    _reg(dma_reg, DMA_DEBUG) = 7;                                   // Clear error bits

    printk(KERN_INFO "smudrv: starting daq engine\n");
    daq_status = DAQ_ACTIVE;
    _reg(dma_reg, DMA_CS) = DMA_CS_ACTIVE | DMA_CS_PRTY(16);        // Start DMA
}


/**
 * @brief Stop DAQ
 *
 * Stop DAQ finite state machine
 */
void stop_daq(void) {
    if (!daq_status)
        return;

    printk(KERN_INFO "smudrv: halting daq engine\n");
    daq_status = DAQ_IDLE;
    _reg(pwm_reg, PWM_CTL) = 0;                                     // Stop PWM
    _reg(dma_reg, DMA_CS) &= ~DMA_CS_ACTIVE;                        // Stop DMA
    _reg(smi_reg, SMI_CS) &= ~SMI_CS_ENABLE;                        // Disable SMI
}
