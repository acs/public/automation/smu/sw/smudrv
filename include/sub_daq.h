// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef SUB_DAQ_H
#define SUB_DAQ_H

#include <linux/kernel.h>                                           // Contains types, macros, functions for the kernel
#include <linux/module.h>                                           // Core header for loading LKMs into the kernel
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/spi/spidev.h>
#include <linux/fs.h>
#include <linux/ioctl.h>
#include <linux/uaccess.h>

#include <rpi_clk_defs.h>
#include <rpi_mem_defs.h>
#include <rpi_pwm_defs.h>
#include <rpi_gpio_defs.h>

#define __NO_VERSION__                                              // Not kernel module main file

#define FS_MAX      200                                             // Max sample rate (kHz)
#define FB_MAX      200                                             // Max buffer rate (Hz)
#define SAMPLE_SIZE 16                                              // Bytes per multi-channel sample

#define DAQ_IDLE    0
#define DAQ_ACTIVE  1

/**
 * @brief  DAQ configuration
 * 
 * Device configuration structure.
 */
typedef struct
{
    uint8_t fs;                                                     // Sample rate (kHz)
    uint8_t fb;                                                     // Buffer rate (Hz)
    uint8_t mode;                                                   // Acquisition mode
    uint8_t sync;                                                   // Synchronization
} __attribute__((__packed__)) smu_conf_t;

/**
 * @brief  DAQ acquisition mode
 */
typedef enum {
    MODE_ONESHOT = 0,                                               // Stop after 1 s
    MODE_FREERUN = 1,                                                   // Continuous
    MODE_VIRTUAL = 2,                                                   // Virtual (MODE_FREERUN & SYNC_NONE)
    MODE_MAX = 3
} smu_mode_t;

/**
 * @brief  DAQ synchronization
 */
typedef enum {
    SYNC_NONE = 0,                                                  // No sync signal
    SYNC_PPS = 1,                                                       // Pulse Per Second
    SYNC_NTP = 2,                                                       // Network Time Protocol
    SYNC_MAX = 3
} smu_sync_t;


/**
 * @brief  Global variables and methods
 */
extern smu_conf_t smu_conf;

extern uint32_t buff_pos;                                           // Buffer head position
extern uint32_t buff_step;                                          // Buffer head increment 

extern int  init_daq(void);
extern int  conf_daq(void);
extern void dest_daq(void);
extern void play_daq(void);
extern void stop_daq(void);

#endif
