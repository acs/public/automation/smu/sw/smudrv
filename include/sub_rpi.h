// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef SMU_RPI_H
#define SMU_RPI_H

#include <linux/kernel.h>                                           // Contains types, macros, functions for the kernel
#include <linux/module.h>                                           // Core header for loading LKMs into the kernel
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/timekeeping.h>

#include <rpi_clk_defs.h>
#include <rpi_mem_defs.h>
#include <rpi_smi_defs.h>
#include <rpi_gpio_defs.h>

#define __NO_VERSION__                                              // Not kernel module main file

/**
 * @brief  GPIO definitions
 * 
 * Here are defined the GPIO used by the SMU hardware
 **/
#define SMI_D0              8
#define SMI_DN              8                                       // GPIOs 8 to 15
#define SMI_SOE             6
#define SMI_SWE             7
#define SMI_DREQ            24
#define SMI_DREQACK         25

#define ADC_RESET           16
#define ADC_FRAME           18
#define ADC_CONVT           19
#define ADC_RANGE           20
#define ADC_STDBY           26

#define IRQ_DMA             17
#define IRQ_PPS             4

#define MUX_VREF            21                                      // Mux select VREF(0) / AI0 (1)
#define MUX_UART            27                                      // Mux select UART(0) / SMI (1)

/**
 * @brief  SMI definitions
 * 
 * SMI configuration values
 */
#define SMI_BITS            SMI_8_BITS
#define SMI_STEP            2
#define SMI_SETUP           1                                       // Tclk = step*(setup+strobe+hold) ns
#define SMI_STROBE          8                                       // Tclk 26 ns
#define SMI_HOLD            4                                       // Fclk 38 MHz


/**
 * @brief  Global functions
 * 
 * Configuration and initialization routines
 */

//extern unsigned long time[2];

extern void gpio_mode(int pin, int mode);
extern void gpio_out (int pin, int val);

extern int  init_gpio(void);
extern int  init_smi (void);
extern int  init_irq (void* ptr);

extern void dest_gpio(void);
extern void dest_smi (void);
extern void dest_irq (void);

#endif
