// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef RPI_DMA_DEFS_H
#define RPI_DMA_DEFS_H

#include <rpi_mem_defs.h>

// DMA register definitions
#define DMA_CHAN        1

#define DMA_PHYS_BASE   (PHYS_REG_BASE + 0x007000)
#define DMA_BUS_BASE    (BUS_REG_BASE + 0x007000)
#define DMA_CS          (DMA_CHAN*0x100)
#define DMA_CONBLK_AD   (DMA_CHAN*0x100 + 0x04)
#define DMA_TI          (DMA_CHAN*0x100 + 0x08)
#define DMA_SRCE_AD     (DMA_CHAN*0x100 + 0x0c)
#define DMA_DEST_AD     (DMA_CHAN*0x100 + 0x10)
#define DMA_TXFR_LEN    (DMA_CHAN*0x100 + 0x14)
#define DMA_STRIDE      (DMA_CHAN*0x100 + 0x18)
#define DMA_NEXTCONBK   (DMA_CHAN*0x100 + 0x1c)
#define DMA_DEBUG       (DMA_CHAN*0x100 + 0x20)
#define DMA_ENABLE      0xff0
#define DMA_INT_STATUS  0xfe0
#define DMA_REGLEN      (DMA_INT_STATUS + 4)


// DMA register values
#define DMA_CS_ACTIVE   (1 << 0)
#define DMA_CS_END      (1 << 1)
#define DMA_CS_INT      (1 << 2)
#define DMA_CS_PRTY(n)  (n << 16)
#define DMA_CS_PPRTY(n) (n << 21)
#define DMA_CS_RESET    (1 << 31)


#define DMA_WAIT_RESP   (1 << 3)
#define DMA_DEST_INC    (1 << 4)
#define DMA_DEST_DREQ   (1 << 6)
#define DMA_SRCE_INC    (1 << 8)
#define DMA_SRCE_DREQ   (1 << 10)
#define DMA_PERMAP(n)   (n << 16)
#define DMA_WAITS(n)    (n << 21)

// DMA control block
typedef struct
{
    uint32_t ti;            // Transfer Information
    uint32_t srce_ad;       // Source Address
    uint32_t dest_ad;       // Destination Address
    uint32_t tfr_len;       // Transfer length
    uint32_t stride;        // 2D Mode Stride
    uint32_t next_cb;       // Next Control Block
    uint32_t debug;         // Debug
    uint32_t unused;
} dma_cb_t __attribute__ ((aligned(32)));

#endif