// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef RPI_PWM_DEFS_H
#define RPI_PWM_DEFS_H

#include <rpi_mem_defs.h>

// PWM Register definitions
#define PWM_PHYS_BASE   (PHYS_REG_BASE + 0x20C000)
#define PWM_BUS_BASE    (BUS_REG_BASE  + 0x20C000)

#define PWM_CTL         0x00        // Control
#define PWM_STA         0x04        // Status
#define PWM_DMAC        0x08        // DMA control
#define PWM_RNG1        0x10        // Channel 1 range
#define PWM_DAT1        0x14        // Channel 1 data
#define PWM_FIF1        0x18        // Channel 1 fifo
#define PWM_RNG2        0x20        // Channel 2 range
#define PWM_DAT2        0x24        // Channel 2 data
#define PWM_REGLEN      (PWM_DAT2 + 4)

// PWM register values
#define PWM_CTL_PWEN1   (1 << 0)    // Chan 1: Enable
#define PWM_CTL_MODE1   (1 << 1)    // Chan 1: Mode
#define PWM_CTL_RPTL1   (1 << 2)    // Chan 1: Repeat Last Data
#define PWM_CTL_SBIT1   (1 << 3)    // Chan 1: Silence Bit
#define PWM_CTL_POLA1   (1 << 4)    // Chan 1: Polarity
#define PWM_CTL_USEF1   (1 << 5)    // Chan 1: Use FIFO
#define PWM_CTL_CLRF1   (1 << 6)    // Chan 1: Clear FIFO
#define PWM_CTL_MSEN1   (1 << 7)    // Chan 1: M/S Enable
#define PWM_CTL_PWEN2   (1 << 8)
#define PWM_CTL_MODE2   (1 << 9)
#define PWM_CTL_RPTL2   (1 << 10)
#define PWM_CTL_SBIT2   (1 << 11)
#define PWM_CTL_POLA2   (1 << 12)
#define PWM_CTL_USEF2   (1 << 13)
#define PWM_CTL_MSEN2   (1 << 15)

#define PWM_STA_FULL1   (1 << 0)    // FIFO Full
#define PWM_STA_EMPT1   (1 << 1)    // FIFO Empty
#define PWM_STA_WERR1   (1 << 2)    // FIFO Write Error
#define PWM_STA_RERR1   (1 << 3)    // FIFO Read Error
#define PWM_STA_BERR    (1 << 8)    // Bus Error

#define DMA_DMAC_DREQ(n)    ((n & 0xFF) << 0)
#define DMA_DMAC_PANIC(n)   ((n & 0xFF) << 8)
#define PWM_DMAC_ENAB       (1 << 31) // Start PWM DMA

#endif