// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef RPI_SMI_DEFS_H
#define RPI_SMI_DEFS_H

#include <rpi_mem_defs.h>

// SMI Register definitions
#define SMI_PHYS_BASE   (PHYS_REG_BASE + 0x600000)
#define SMI_BUS_BASE    (BUS_REG_BASE + 0x600000)
#define SMI_CS          0x00    // Control & status
#define SMI_L           0x04    // Transfer length
#define SMI_A           0x08    // Address
#define SMI_D           0x0c    // Data
#define SMI_DSR0        0x10    // Read settings device 0
#define SMI_DSW0        0x14    // Write settings device 0
#define SMI_DSR1        0x18    // Read settings device 1
#define SMI_DSW1        0x1c    // Write settings device 1
#define SMI_DSR2        0x20    // Read settings device 2
#define SMI_DSW2        0x24    // Write settings device 2
#define SMI_DSR3        0x28    // Read settings device 3
#define SMI_DSW3        0x2c    // Write settings device 3
#define SMI_DC          0x30    // DMA control
#define SMI_DCS         0x34    // Direct control/status
#define SMI_DCA         0x38    // Direct address
#define SMI_DCD         0x3c    // Direct data
#define SMI_FD          0x40    // FIFO debug
#define SMI_REGLEN      (SMI_FD + 4)

// Data widths
#define SMI_8_BITS      0
#define SMI_16_BITS     1
#define SMI_18_BITS     2
#define SMI_9_BITS      3

// DMA request
#define DMA_SMI_DREQ    4

// SMI register values
#define SMI_CS_ENABLE       (1 << 0)
#define SMI_CS_DONE         (1 << 1)
#define SMI_CS_ACTIVE       (1 << 2)
#define SMI_CS_START        (1 << 3)
#define SMI_CS_CLEAR        (1 << 4)
#define SMI_CS_WRITE        (1 << 5)
#define SMI_CS_PAD(n)       ((n & 0x03) << 6)
#define SMI_CS_TEEN         (1 << 8)
#define SMI_CS_INTD         (1 << 9)
#define SMI_CS_INTT         (1 << 10)
#define SMI_CS_INTR         (1 << 11)
#define SMI_CS_PVMODE       (1 << 12)
#define SMI_CS_SETERR       (1 << 13)
#define SMI_CS_PXLDAT       (1 << 14)
#define SMI_CS_EDREQ        (1 << 15)
#define SMI_CS_PRDY         (1 << 24)
#define SMI_CS_AFERR        (1 << 25)
#define SMI_CS_TXW          (1 << 26)
#define SMI_CS_RXR          (1 << 27)
#define SMI_CS_TXD          (1 << 28)
#define SMI_CS_RXD          (1 << 29)
#define SMI_CS_TXE          (1 << 30)
#define SMI_CS_RXF          (1 << 31)

#define SMI_A_ADDR(n)       ((n & 0x3F) << 0)
#define SMI_A_DEVICE(n)     ((n & 0x03) << 8)

#define SMI_DC_REQW(n)      ((n & 0x3F) << 0)
#define SMI_DC_REQR(n)      ((n & 0x3F) << 6)
#define SMI_DC_PANICW(n)    ((n & 0x3F) << 12)
#define SMI_DC_PANICR(n)    ((n & 0x3F) << 18)
#define SMI_DC_DMAP         (1 << 24)
#define SMI_DC_DMAEN        (1 << 28)

#define SMI_DSR_RSTROBE(n)  ((n & 0x7F) << 0)
#define SMI_DSR_RDREQ       (1 << 7)
#define SMI_DSR_RPACE(n)    ((n & 0x7F) << 8)
#define SMI_DSR_RPACEALL    (1 << 15)
#define SMI_DSR_RHOLD(n)    ((n & 0x3F) << 16)
#define SMI_DSR_FSETUP      (1 << 22)
#define SMI_DSR_MODE68      (1 << 23)
#define SMI_DSR_RSETUP(n)   ((n & 0x3F) << 24)
#define SMI_DSR_RWIDTH(n)   ((n & 0x03) << 30)

#define SMI_DSW_WSTROBE(n)  ((n & 0x7F) << 0)
#define SMI_DSW_WDREQ       (1 << 7)
#define SMI_DSW_WPACE(n)    ((n & 0x7F) << 8)
#define SMI_DSW_WPACEALL    (1 << 15)
#define SMI_DSW_WHOLD(n)    ((n & 0x3F) << 16)
#define SMI_DSW_WSWAP       (1 << 22)
#define SMI_DSW_WFORMAT     (1 << 23)
#define SMI_DSW_WSETUP(n)   ((n & 0x3F) << 24)
#define SMI_DSW_WWIDTH(n)   ((n & 0x03) << 30)

#endif