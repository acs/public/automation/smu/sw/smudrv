// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef RPI_MEM_DEFS_H
#define RPI_MEM_DEFS_H

// MEM register definitions
#define BUS_REG_BASE            0x7E000000
#define PHYS_REG_BASE           0x3F000000

// VC CPU Bus Addresses offset
#define VC_0_L1_L2_CACHED       0x00000000                          // '0' Alias - L1 and L2 cached
#define VC_0_L2_CACHED_COHERENT 0x40000000                          // '4' Alias - L2 cache coherent (non allocating)
#define VC_0_L2_CACHED_ONLY     0x80000000                          // '8' Alias - L2 cached (only)
#define VC_0_DIRECT_UNCACHED    0xC0000000                          // 'C' Alias - direct uncached

#define _reg(base, reg)         (*(volatile uint32_t *)(base + reg))
#define _v2b(virt, bus, mem)    ((uint32_t)mem  - \
                                 (uint32_t)virt + \
                                 (uint32_t)bus  + \
                                 VC_0_DIRECT_UNCACHED)    // Map mem address from virtual to bus memory

#endif