// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#include "sub_daq.h"
#include "sub_dev.h"
#include "sub_rpi.h"

static int smuDRV_open(struct inode *inode, struct file *file);
static int smuDRV_release(struct inode *inode, struct file *file);
static int smuDRV_mmap(struct file *file, struct vm_area_struct *vma);
static long smuDRV_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
static ssize_t smuDRV_read(struct file *file, char __user *buf, size_t len, loff_t *off);
static ssize_t smuDRV_write(struct file *file, const char *buf, size_t len, loff_t *off);


static dev_t dev = 0;
static struct class *smu_class;
static struct device *smu_device;
static struct cdev smu_cdev;
static struct task_struct *task = NULL;
static struct timespec64 time;

DEFINE_MUTEX(smudrv_mutex);

static const struct file_operations fops =
    {
        .owner = THIS_MODULE,
        .open = smuDRV_open,
        .release = smuDRV_release,
        .mmap = smuDRV_mmap,
        .read = smuDRV_read,
        .write = smuDRV_write,
        .unlocked_ioctl = smuDRV_ioctl,
};

void *sh_data = NULL; dma_addr_t dma_data;
void *sh_conf = NULL; dma_addr_t dma_conf;
volatile void *clk_reg, *dma_reg, *pio_reg, *pwm_reg, *smi_reg;

/**
 * @brief Initialize Memory
 *
 * This function allocates and initialize shared
 * memory, bus memory and peripheral registers
 *
 * @return ret, error code
 */
int init_mem(void)
{
    int i, ret = 0;
    /// Map registers
    clk_reg = (volatile void *)ioremap(CLK_PHYS_BASE, CLK_REGLEN);
    dma_reg = (volatile void *)ioremap(DMA_PHYS_BASE, DMA_REGLEN);
    pio_reg = (volatile void *)ioremap(GPIO_PHYS_BASE, GPIO_REGLEN);
    pwm_reg = (volatile void *)ioremap(PWM_PHYS_BASE, PWM_REGLEN);
    smi_reg = (volatile void *)ioremap(SMI_PHYS_BASE, SMI_REGLEN);

    /// Create dma coherent data area
    sh_data = dma_alloc_coherent(smu_device, MMAP_SIZE, &dma_data, GFP_KERNEL);
    if (dma_mapping_error(smu_device, dma_data) || (sh_data == NULL))
    {
        printk(KERN_ERR "smudrv: failed to allocate data memory\n");
        ret = -ENOMEM;
        goto r_out;
    }
    for (i = 0; i < MMAP_SIZE; i += PAGE_SIZE)
        SetPageReserved(virt_to_page(((unsigned long)sh_data) + i));
    memset(sh_data, 0, MMAP_SIZE);

    /// Create dma coherent config area
    sh_conf = dma_alloc_coherent(smu_device, PAGE_SIZE, &dma_conf, GFP_KERNEL);
    if (dma_mapping_error(smu_device, dma_conf) || (sh_conf == NULL))
    {
        printk(KERN_ERR "smudrv: failed to allocate config memory\n");
        ret = -ENOMEM;
        goto r_data;
    }
    SetPageReserved(virt_to_page((unsigned long)sh_conf));
    memset(sh_conf, 0, PAGE_SIZE);

    return 0;

r_data:
    for (i = 0; i < MMAP_SIZE; i += PAGE_SIZE)
        ClearPageReserved(virt_to_page(((unsigned long)sh_data) + i));
    dma_free_coherent(smu_device, MMAP_SIZE, sh_data, dma_data);
r_out:
    return ret;
}

/**
 * @brief Destroy Memory
 *
 * This function frees the memory resources
 */
void dest_mem(void)
{
    int i;

    /// Free mmap and config area
    for (i = 0; i < MMAP_SIZE; i += PAGE_SIZE)
        ClearPageReserved(virt_to_page(((unsigned long)sh_data) + i));
    ClearPageReserved(virt_to_page((unsigned long)sh_conf));
    dma_free_coherent(smu_device, MMAP_SIZE, sh_data, dma_data);
    dma_free_coherent(smu_device, PAGE_SIZE, sh_conf, dma_conf);

    /// Unmap registers
    iounmap(clk_reg);
    iounmap(dma_reg);
    iounmap(pio_reg);
    iounmap(pwm_reg);
    iounmap(smi_reg);
}

/**
 * @brief Init device
 *
 * Init the character device's driver.
 *
 * @return ret, error code
 */
int dev_init(void)
{
    int major, ret = 0;

    /// Register Char Device
    major = alloc_chrdev_region(&dev, 0, 1, "smuDRV_dev");
    if (major < 0)
    {
        printk(KERN_ERR "smudrv: fail to register major number\n");
        ret = major;
        goto r_out;
    }

    /// Create cdev structure
    cdev_init(&smu_cdev, &fops);

    /// Add character device to the system
    if ((cdev_add(&smu_cdev, dev, 1)) < 0)
    {
        printk(KERN_ERR "smudrv: fail to add the device to the system\n");
        goto r_cdev;
    }

    /// Create struct class
    smu_class = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(smu_class))
    {
        printk(KERN_ERR "smudrv: failed to register device class\n");
        goto r_class;
    }

    /// Create device
    smu_device = device_create(smu_class, NULL, dev, NULL, DEVICE_NAME);
    if (IS_ERR(smu_device))
    {
        printk(KERN_ERR "smudrv: failed to create device class\n");
        goto r_device;
    }

    /// Init device mutex
    mutex_init(&smudrv_mutex);

    /// Init Memory
    ret = init_mem();
    if (ret < 0)
    {
        printk(KERN_ERR "smudrv: fail to initialize memory\n");
        goto r_mem;
    }

    /// Init GPIOs
    ret = init_gpio();
    if (ret < 0)
    {
        printk(KERN_ERR "smudrv: fail to initialize gpio\n");
        goto r_gpio;
    }

    /// Init SMI
    ret = init_smi();
    if (ret < 0)
    {
        printk(KERN_ERR "smudrv: fail to initialize smi\n");
        goto r_smi;
    }

    /// Init IRQs
    ret = init_irq((void*)&time);
    if (ret < 0)
    {
        printk(KERN_ERR "smudrv: fail to initialize irq\n");
        goto r_irq;
    }

    /// Init DAQ
    ret = init_daq();
    if (ret < 0)
    {
        printk(KERN_ERR "smudrv: fail to initialize daq\n");
        goto r_daq;
    }

    printk(KERN_INFO "smudrv: char driver initialized <major: %d, minor: %d>\n", MAJOR(dev), MINOR(dev));

    return 0;

r_daq:
    dest_irq();
r_irq:
    dest_smi();
r_smi:
    dest_gpio();
r_gpio:
    dest_mem();
r_mem:
    mutex_destroy(&smudrv_mutex);
    device_destroy(smu_class,dev);
r_device:
    class_unregister(smu_class);
    class_destroy(smu_class);
r_class:
    cdev_del(&smu_cdev);
r_cdev:
    unregister_chrdev_region(dev, 1);
r_out:
    return ret;
}

/**
 * @brief Destroy device
 *
 * Destroys the character device's driver.
 */
void dev_exit(void)
{
    /// Stop data acquisition
    stop_daq();

    /// Destroy submodules
    dest_daq();
    dest_irq();
    dest_smi();
    dest_gpio();
    dest_mem();

    /// Destroy device mutex
    mutex_destroy(&smudrv_mutex);

    /// Destroy device
    device_destroy(smu_class, dev);

    /// Destroy struct class
    class_unregister(smu_class);
    class_destroy(smu_class);

    /// Remove character device from the system
    cdev_del(&smu_cdev);

    /// Unregister Char Device
    unregister_chrdev_region(dev, 1);
}

/**
 * @brief Device emit
 *
 * Emit a signal that can be captured by the user space
 * 
 * @param si_signo  signal number
 * @param si_val  signal value
 */
void dev_emit(int si_signo, int si_val)
{
    struct kernel_siginfo info;
    memset(&info, 0, sizeof(struct kernel_siginfo));
    info.si_signo = si_signo;
    info.si_code = SI_QUEUE;
    info.si_int = si_val;

    if (task != NULL)
        if (send_sig_info(si_signo, &info, task) < 0)
            printk(KERN_INFO "smudrv: fail to send signal\n");
}

/**
 * @brief Open device
 *
 * Defines the actions when the driver is opened.
 * 
 * @param inode from userspace
 * @param file  file pointer
 *
 * @return ret, error code
 */
static int smuDRV_open(struct inode *inode, struct file *file)
{
    if (!mutex_trylock(&smudrv_mutex))
    {
        printk(KERN_ERR "smudrv: cannot open device busy\n");
        mutex_unlock(&smudrv_mutex);
        return -EBUSY;
    }

    return 0;
}

/**
 * @brief Release device
 *
 * Defines the actions when the driver is released
 * by the userspace program.
 * 
 * @param inode from userspace
 * @param file  file pointer
 *
 * @return ret, error code
 */
int smuDRV_release(struct inode *inode, struct file *file)
{
    mutex_unlock(&smudrv_mutex);

    return 0;
}

/**
 * @brief Read device data (legacy)
 *
 * Defines the actions when the driver is opened
 * by the userspace program in read mode.
 * 
 * @param file file pointer
 * @param buf  data buffer
 * @param len  buffer length
 * @param off  buffer offset
 *
 * @return bytes read or error code
 */
static ssize_t smuDRV_read(struct file *file, char __user *buf, size_t len, loff_t *off)
{
    char *ch_mem = (char *)sh_data;

    /// Write data to user space
    if (copy_to_user(buf, &ch_mem[*off], len))
    {
        printk(KERN_ERR "smudrv: io_read error, failed to send data to the user\n");
        return -EFAULT; // Failed -- return a bad address message (i.e. -14)
    }

    return 0;
}

/**
 * @brief Write device configuration
 *
 * Defines the actions when the driver is opened
 * by the userspace program in write mode.
 * 
 * @param file file pointer
 * @param buf  data buffer
 * @param len  buffer length
 * @param off  buffer offset
 *
 * @return bytes written or error code
 */
static ssize_t smuDRV_write(struct file *file, const char *buf, size_t len, loff_t *off)
{
    /// Read data from user space
    if (copy_from_user(&smu_conf, buf, sizeof(smu_conf_t)))
    {
        printk(KERN_ERR "smudrv: io_write error, failed to receive smu_conf\n");
        return -EFAULT;
    }

    return 0;
}

/**
 * @brief Memory Map device
 *
 * Performs the memory mapping between kernal and user space.
 * 
 * @param file file pointer
 * @param vma  virtual memory area
 *
 * @return err, error code
 */
static int smuDRV_mmap(struct file *file, struct vm_area_struct *vma)
{
    int ret = 0;
    struct page *page = NULL;
    unsigned long size = (unsigned long)(vma->vm_end - vma->vm_start);

    if (size > MMAP_SIZE)
    {
        printk(KERN_ERR "smudrv: map area out of bounds\n");
        ret = -EINVAL;
        goto out;
    }

    /// Avoid mmap from caching values
    vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);

    /// Map shared memory to user space
    page = virt_to_page((unsigned long)sh_data + (vma->vm_pgoff << PAGE_SHIFT));
    ret = remap_pfn_range(vma, vma->vm_start, page_to_pfn(page), size, vma->vm_page_prot);
    if (ret < 0)
    {
        printk(KERN_ERR "smudrv: fail to map the address area\n");
        goto out;
    }

out:
    return ret;
}

/**
 * @brief IO Control
 *
 * Handles IOCTL commands from the user space.
 * 
 * @param file file pointer
 * @param cmd  command code
 * @param arg  command argument
 *
 * @return err, error code
 */
static long smuDRV_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int ret = 0;

    switch (cmd)
    {
    case SMU_IOC_REG_TASK:
        task = get_current();
        printk(KERN_INFO "smudrv: ioctl register task (pid %d)\n",task->pid);
        break;

    case SMU_IOC_RESET:
        printk(KERN_INFO "smudrv: ioctl reset sequence starting..\n");
        dev_exit();
        dev_init();
        break;

    case SMU_IOC_START:
        play_daq();
        break;

    case SMU_IOC_STOP:
        stop_daq();
        break;

    case SMU_IOC_GET_TIME:
        if (copy_to_user((void *)arg, &time, sizeof(time64_t)+sizeof(long)))
            printk(KERN_ERR "smudrv: ioctl get kernel time failed\n");
        break;

    case SMU_IOC_SET_CONF:
        if (copy_from_user(&smu_conf, (void *)arg, sizeof(smu_conf_t)))
            printk(KERN_ERR "smudrv: ioctl set configuration failed\n");
        break;

    case SMU_IOC_GET_CONF:
        if (copy_to_user((void *)arg, &smu_conf, sizeof(smu_conf_t)))
            printk(KERN_ERR "smudrv: ioctl get configuration failed\n");
        break;

    default:
        printk(KERN_ERR "smudrv: invalid ioctl cmd: %d\n", cmd);
        ret = -EINVAL;
        break;
    }
    return ret;
}
