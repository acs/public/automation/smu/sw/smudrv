// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef RPI_GPIO_DEFS_H
#define RPI_GPIO_DEFS_H

#include <rpi_mem_defs.h>

// GPIO register definitions
#define GPIO_PHYS_BASE  (PHYS_REG_BASE + 0x200000)
#define GPIO_BUS_BASE   (BUS_REG_BASE + 0x200000)
#define GPIO_GPFSEL0    0x00    // GPIO Function SelecT
#define GPIO_GPFSEL(n)  (GPIO_GPFSEL0 + (n / 10) * 0x04)

#define GPIO_SET0       0x1c    // GPIO Pin Output Set
#define GPIO_SET1       0x20
#define GPIO_CLR0       0x28    // GPIO Pin Output Clear
#define GPIO_CLR1       0x2C
#define GPIO_LEV0       0x34    // GPIO Pin Level
#define GPIO_LEV1       0x38
#define GPIO_EDS0       0x40    // GPIO Pin Event Detect Status
#define GPIO_EDS1       0x44
#define GPIO_REN0       0x4C    // GPIO Pin Rising Edge Detect Enable
#define GPIO_REN1       0x50
#define GPIO_FEN0       0x58    // GPIO Pin Falling Edge Detect Enable
#define GPIO_FEN1       0x5C
#define GPIO_HEN0       0x64    // GPIO Pin High Detect Enable
#define GPIO_HEN1       0x68
#define GPIO_LEN0       0x70    // GPIO Pin Low Detect Enable
#define GPIO_LEN1       0x74
#define GPIO_AREN0      0x7C    // GPIO Pin Async. Rising Edge Detect
#define GPIO_AREN1      0x80
#define GPIO_AFEN0      0x88    // GPIO Pin Async. Falling Edge Detect
#define GPIO_AFEN1      0x8C
#define GPIO_REG(r,n)   (r + (n / 32) * 0x04)

#define GPIO_GPPUD      0x94    // GPIO Pin Pull-up/down Enable
#define GPIO_GPPUDCLK0  0x98    // GPIO Pin Pull-up/down Enable Clock
#define GPIO_GPPUDCLK1  0x9C
#define GPIO_REGLEN     (GPIO_GPPUDCLK1 + 4)

// GPIO I/O definitions
#define GPIO_PIN(n)     (1 << (n % 32))
#define GPIO_FSEL(m,n)  (m << (n % 10) * 3)
#define GPIO_IN         0
#define GPIO_OUT        1
#define GPIO_ALT0       4
#define GPIO_ALT1       5
#define GPIO_ALT2       6
#define GPIO_ALT3       7
#define GPIO_ALT4       3
#define GPIO_ALT5       2

#define GPIO_NOPULL     0
#define GPIO_PULLDN     1
#define GPIO_PULLUP     2

#define LOW             0
#define HIGH            1

#endif