// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef SUB_DEV_H
#define SUB_DEV_H

#include <linux/module.h>                                           // Core header for loading LKMs into the kernel
#include <linux/kernel.h>                                           // Contains types, macros, functions for the kernel
#include <linux/device.h>
#include <linux/uaccess.h>                                          // copy_to/from_user()
#include <linux/cdev.h>                                             // cdev_init/cdev_add/cdev_del
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/sched/signal.h>
#include <linux/dma-mapping.h>
#include <linux/timekeeping.h>

#include <rpi_clk_defs.h>                                           // Hardware descriptors from datasheet
#include <rpi_dma_defs.h>
#include <rpi_pwm_defs.h>
#include <rpi_smi_defs.h>
#include <rpi_gpio_defs.h>

#define __NO_VERSION__                                              // Not kernel module main file

#define PAGE_NUM        800                                         // Enough for 1 s * 200 kSPs * 8 ch * 2 byte/sample/ch
#define MMAP_SIZE       (PAGE_SIZE * PAGE_NUM)
#define DEVICE_NAME     "smu"
#define  CLASS_NAME     "smu_class"

/**
 * @brief  Memory 
 * 
 * Shared memory, bus memory and peripheral registries.
 */
extern void *sh_data;                                               // Data (virtual mem)
extern void *sh_conf;                                               // Conf (virtual mem)
extern dma_addr_t dma_data;                                         // Data (bus mem)
extern dma_addr_t dma_conf;                                         // Conf (bus mem)

//extern long time[2];                                                // Time Information


extern volatile void *clk_reg;                                      // Clock Manager
extern volatile void *dma_reg;                                      // Direct Memory Access
extern volatile void *pio_reg;                                      // General Purpose IO
extern volatile void *pwm_reg;                                      // Pulse Width Modulation
extern volatile void *smi_reg;                                      // Secondary Memory Interface

/**
 * @brief  IOCTL definitions
 * 
 * Here are defined the different IOCTL commands used
 * to communicate from the userspace to the kernel module.
 */
#define SMU_IOC_MAGIC           'k'
#define SMU_IOC_REG_TASK        _IO (SMU_IOC_MAGIC, 0)
#define SMU_IOC_RESET           _IO (SMU_IOC_MAGIC, 1)
#define SMU_IOC_START           _IO (SMU_IOC_MAGIC, 2)
#define SMU_IOC_STOP            _IO (SMU_IOC_MAGIC, 3)
#define SMU_IOC_GET_TIME        _IOR(SMU_IOC_MAGIC, 4, struct timespec64*)
#define SMU_IOC_SET_CONF        _IOW(SMU_IOC_MAGIC, 5, smu_conf_t*)
#define SMU_IOC_GET_CONF        _IOR(SMU_IOC_MAGIC, 6, smu_conf_t*)
#define SMU_IO_MAX_NUM          7

/**
 * @brief  Signal definitions
 * 
 * Here are defined the different values used to send
 * signals to the userspace
 */
#define SMU_SIG_SYNC            41
#define SMU_SIG_DATA            42

/**
 * @brief  Methods
 * 
 * Character device external methods.
 */
extern int  dev_init(void);
extern void dev_exit(void);
extern void dev_emit(int si_signo, int si_val);

#endif
