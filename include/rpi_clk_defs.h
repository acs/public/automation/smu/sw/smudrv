// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef RPI_CLK_DEFS_H
#define RPI_CLK_DEFS_H

#include <rpi_mem_defs.h>

// CLK Register definitions
#define CLK_PHYS_BASE   (PHYS_REG_BASE + 0x101000)
#define CLK_BUS_BASE    (BUS_REG_BASE  + 0x101000)

#define CLK_PCM_CTL     0x98
#define CLK_PCM_DIV     0x9c
#define CLK_PWM_CTL     0xa0
#define CLK_PWM_DIV     0xa4
#define CLK_SMI_CTL     0xb0
#define CLK_SMI_DIV     0xb4
#define CLK_REGLEN      0x1000

// CLK Register values
#define CLK_CTL_SRC(n)  ((n & 0x0F) << 0)   // Clock source
#define CLK_CTL_ENAB    (1 << 4)            // Enable the clock generator
#define CLK_CTL_KILL    (1 << 5)            // Kill the clock generator
#define CLK_CTL_BUSY    (1 << 7)            // Clock generator is running
#define CLK_CTL_FLIP    (1 << 8)            // Invert the clock generator output
#define CLK_CTL_MASH(n) ((n & 0x03) << 9)   // MASH control

#define CLK_DIV_DIVF(n) ((n & 0xFFF) << 0)  // Fractional part of divisor
#define CLK_DIV_DIVI(n) ((n & 0xFFF) << 12) // Integer part of divisor

#define CLK_PASSWD      0x5a000000

// CLK Sources
#define CLK_SRC_GND     0       // 0 Hz
#define CLK_SRC_CXO     1       // 19.2 MHz
#define CLK_SRC_DBG0    2
#define CLK_SRC_DBG1    3
#define CLK_SRC_PLLA    4
#define CLK_SRC_PLLC    5       // 1000 MHz (changes with overclock settings)
#define CLK_SRC_PLLD    6       //  500 MHz
#define CLK_SRC_HDMI    7       //  216 MHz

// CLK frequencies (kHz)
#define CLK_F_CXO       19200UL
#define CLK_F_PLLC      1000000UL
#define CLK_F_PLLD      500000UL

#endif