// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#include "sub_daq.h"
#include "sub_dev.h"
#include "sub_rpi.h"

static irqreturn_t PPS_irq_handler(int irq,void *dev_id);
static irqreturn_t DMA_irq_handler(int irq, void *dev_id);
static irqreturn_t threaded_PPS_irq_handler(int irq, void *dev_id);
static irqreturn_t threaded_DMA_irq_handler(int irq, void *dev_id);

static struct gpio signals[] =
{
    {IRQ_PPS, GPIOF_IN, "PPS Sync Signal" },
    {IRQ_DMA, GPIOF_IN, "DMA Buff Signal" },
};
static int gpio_irqs[] = { -1, -1 };

static struct timespec64 *time_ptr;

/**
 * @brief PPS irq handler
 *
 * This function is a hard interrupt handler that 
 * calls the respective soft interrupt thread.
 *
 * @param irq  interrupt pin
 * @param dev_id  device id pointer
 *
 * @return irqreturn_t
 */
static irqreturn_t PPS_irq_handler(int irq,void *dev_id)
{
    // Get systim    
    ktime_get_real_ts64(time_ptr); 
    buff_pos = 0;
    return IRQ_WAKE_THREAD;
}

/**
 * @brief DMA irq handler
 *
 * This function is a hard interrupt handler that 
 * calls the respective soft interrupt thread.
 *
 * @param irq  interrupt pin
 * @param dev_id  device id pointer
 *
 * @return irqreturn_t
 */
static irqreturn_t DMA_irq_handler(int irq, void *dev_id)
{
    return IRQ_WAKE_THREAD;
}

/**
 * @brief Threaded PPS irq handler
 *
 * This function is a soft interrupt handler running in a thread 
 * called by the respective hard interrupt routine.
 *
 * @param irq  interrupt pin
 * @param dev_id  device id pointer
 *
 * @return irqreturn_t
 */
static irqreturn_t threaded_PPS_irq_handler(int irq, void *dev_id)
{    
    if (smu_conf.sync == SYNC_PPS)        
        dev_emit(SMU_SIG_SYNC, 0);        
    return IRQ_HANDLED;
}

/**
 * @brief Threaded DMA irq handler
 *
 * This function is a soft interrupt handler running in a thread 
 * called by the respective hard interrupt routine.
 *
 * @param irq  interrupt pin
 * @param dev_id  device id pointer
 *
 * @return irqreturn_t
 */
static irqreturn_t threaded_DMA_irq_handler(int irq, void *dev_id)
{
    /// Chunk complete
    uint32_t buff_offset = ((buff_pos - 1 + smu_conf.fb) % smu_conf.fb)*buff_step;
    dev_emit(SMU_SIG_DATA, buff_offset);    
    buff_pos++;
    return IRQ_HANDLED;
}

/**
 * @brief Configure GPIO
 *
 * This function configures the GPIO mode
 */
void gpio_mode(int pin, int mode)
{
    _reg(pio_reg, GPIO_GPFSEL(pin)) |= GPIO_FSEL(mode, pin);
}

/**
 * @brief Set GPIO
 *
 * This function sets the output value
 */
void gpio_out(int pin, int val)
{
    _reg(pio_reg, GPIO_REG(val ? GPIO_SET0 : GPIO_CLR0, pin)) =  GPIO_PIN(pin);
}


/**
 * @brief Configure and initialize the GPIOs
 *
 * This function requests and configures the GPIOs 
 *
 * @return ret, error code
 */
int init_gpio (void)
{
    int i, ret = 0;
    char label[8];

    /// Request GPIO resource
    ret += gpio_request(MUX_VREF , "MUX_VREF");
    ret += gpio_request(MUX_UART , "MUX_UART");

    ret += gpio_request(ADC_RESET, "ADC_RESET");
    ret += gpio_request(ADC_FRAME, "ADC_FRAME");
    ret += gpio_request(ADC_CONVT, "ADC_CONVT");
    ret += gpio_request(ADC_RANGE, "ADC_RANGE");
    ret += gpio_request(ADC_STDBY, "ADC_STDBY");
    
    ret += gpio_request(SMI_SOE,   "SMI_SOE");
    ret += gpio_request(SMI_DREQ,  "SMI_DREQ");
    for (i = 0; i < SMI_DN; i++){
        sprintf(label, "SMI_D%d", i);
        ret += gpio_request(SMI_D0 + i, label);
    }
    if (ret < 0)
        return ret;

    /// Set GPIO mode
    gpio_mode(MUX_VREF , GPIO_OUT);
    gpio_mode(MUX_UART , GPIO_OUT);

    gpio_mode(ADC_RESET, GPIO_OUT);
    gpio_mode(ADC_FRAME, GPIO_ALT5);
    gpio_mode(ADC_CONVT, GPIO_ALT5);
    gpio_mode(ADC_RANGE, GPIO_OUT);
    gpio_mode(ADC_STDBY, GPIO_OUT);

    gpio_mode(SMI_SOE,   GPIO_ALT1);
    gpio_mode(SMI_DREQ,  GPIO_ALT1);
    for (i = 0; i < SMI_DN; i++)
        gpio_mode(SMI_D0 + i, GPIO_ALT1);

    printk(KERN_INFO "smudrv: gpio configured\n");
    return 0;
}

/**
 * @brief Destroy GPIOs
 *
 * This function frees the requested GPIOs 
 */
void dest_gpio (void)
{
    int i;

    /// Reset GPIO mode
    gpio_mode(MUX_VREF , GPIO_IN);
    gpio_mode(MUX_UART , GPIO_IN);

    gpio_mode(ADC_RESET, GPIO_IN);
    gpio_mode(ADC_FRAME, GPIO_IN);
    gpio_mode(ADC_CONVT, GPIO_IN);
    gpio_mode(ADC_RANGE, GPIO_IN);
    gpio_mode(ADC_STDBY, GPIO_IN);

    gpio_mode(SMI_SOE,   GPIO_IN);
    gpio_mode(SMI_DREQ,  GPIO_IN);
    for (i = 0; i < SMI_DN; i++)
        gpio_mode(SMI_D0 + i, GPIO_IN);

    /// Free GPIO resource
    gpio_free(MUX_VREF);
    gpio_free(MUX_UART);

    gpio_free(ADC_RESET);
    gpio_free(ADC_FRAME);
    gpio_free(ADC_CONVT);
    gpio_free(ADC_RANGE);
    gpio_free(ADC_STDBY);

    gpio_free(SMI_SOE);
    gpio_free(SMI_DREQ);
    for (i = 0; i < SMI_DN; i++)
        gpio_free(SMI_D0 + i);
}

/**
 * @brief Configure and initialize the SMI
 *
 * This function configures the SMI interface
 *
 * @return ret, error code
 */
int init_smi(void)
{
    uint32_t divi = SMI_STEP / 2;

    /// Reset SMI registers
    _reg(smi_reg, SMI_CS)   = 0;
    _reg(smi_reg, SMI_L)    = 0;
    _reg(smi_reg, SMI_A)    = 0;
    _reg(smi_reg, SMI_DSR0) = 0;
    _reg(smi_reg, SMI_DSW0) = 0;
    _reg(smi_reg, SMI_DCS)  = 0;
    _reg(smi_reg, SMI_DCA)  = 0;

    /// Configure SMI Clock Manager
    _reg(clk_reg, CLK_SMI_CTL) = CLK_PASSWD | CLK_CTL_KILL;         // Kill clock
    udelay(10);
    while ((_reg(clk_reg, CLK_SMI_CTL) & CLK_CTL_BUSY) != 0)        // Wait clock stop
        udelay(10);
    _reg(clk_reg, CLK_SMI_DIV) = CLK_PASSWD |
                                 CLK_DIV_DIVI(divi);                // Set clock divisor
    _reg(clk_reg, CLK_SMI_CTL) = CLK_PASSWD |
                                 CLK_CTL_SRC(CLK_SRC_PLLD);         // PLLD @ 500 MHz
    udelay(10);
    _reg(clk_reg, CLK_SMI_CTL)|= CLK_PASSWD | CLK_CTL_ENAB;         // Start clock
    udelay(10);
    while ((_reg(clk_reg, CLK_SMI_CTL) & CLK_CTL_BUSY) == 0)        // Wait clock start
        udelay(10);

    if (_reg(smi_reg, SMI_CS) & SMI_CS_SETERR)
        _reg(smi_reg, SMI_CS) |= SMI_CS_SETERR;

    /// SMI Read Settings
    _reg(smi_reg, SMI_DSR0) = SMI_DSR_RSETUP(SMI_SETUP) |
                              SMI_DSR_RSTROBE(SMI_STROBE) |
                              SMI_DSR_RHOLD(SMI_HOLD) |
                              SMI_DSR_RWIDTH(SMI_BITS)|
                              SMI_DSR_RDREQ;

    /// SMI Write Settings
    _reg(smi_reg, SMI_DSW0) = SMI_DSW_WSETUP(SMI_SETUP) |
                              SMI_DSW_WSTROBE(SMI_STROBE) |
                              SMI_DSW_WHOLD(SMI_HOLD) |
                              SMI_DSW_WWIDTH(SMI_BITS);

    /// SMI DMA Control Settings
    _reg(smi_reg, SMI_DC)   = SMI_DC_PANICR(2) |
                              SMI_DC_PANICW(2) |
                              SMI_DC_REQR(1) |
                              SMI_DC_REQW(1) |
                              SMI_DC_DMAEN |
                              SMI_DC_DMAP;
    
    /// SMI Control Status
    _reg(smi_reg, SMI_CS)  |= SMI_CS_ENABLE;
    _reg(smi_reg, SMI_CS)  |= SMI_CS_CLEAR;

    if (_reg(smi_reg, SMI_CS) & SMI_CS_SETERR)
        return -EBUSY;
  
    printk(KERN_INFO "smudrv: smi configured\n");
    return 0;    
}

/**
 * @brief Destroy SMI
 *
 * This function frees the SMI resources
 */
void dest_smi(void)
{
    /// Reset SMI interface
    _reg(smi_reg, SMI_CS) = 0;
}


/**
 * @brief Configure and initialize the IRQs
 *
 * This function requests and configures the IRQs 
 *
 * @return ret, error code
 */
int init_irq(void* ptr)
{
    int ret;

    time_ptr = (struct timespec64*)ptr;

    ret = gpio_request_array(signals, ARRAY_SIZE(signals));
    if (ret) {
        printk(KERN_ERR "smudrv: fail to request GPIOs for RX Signals: %d\n", ret);
        return ret;
    }

    /// Register IRQ for PPS Signal
    ret = gpio_to_irq(signals[0].gpio);
    if(ret < 0) {
        printk(KERN_ERR "smudrv: fail to request hard-irq: %d\n", ret);
        return ret;
    }
    gpio_irqs[0] = ret;
    ret = request_threaded_irq(gpio_irqs[0], PPS_irq_handler, threaded_PPS_irq_handler, IRQF_TRIGGER_RISING | IRQF_TIMER, "smu#pps", NULL);
    if(ret < 0) {
        printk(KERN_ERR "smudrv: fail to request soft-irq: %d\n", ret);
        return ret;
    }
    printk(KERN_INFO "smudrv: pps sync irq # %d\n", gpio_irqs[0]);

    /// Register IRQ for DMA Signal
    ret = gpio_to_irq(signals[1].gpio);
    if(ret < 0) {
        printk(KERN_ERR "smudrv: fail to request hard-irq: %d\n", ret);
        return ret;
    }
    gpio_irqs[1] = ret;
    ret = request_threaded_irq(gpio_irqs[1], DMA_irq_handler, threaded_DMA_irq_handler, IRQF_TRIGGER_RISING | IRQF_TIMER, "smu#dma", NULL);
    if(ret < 0) {
        printk(KERN_ERR "smudrv: fail to request soft-irq: %d\n", ret);
        return ret;
    }
    printk(KERN_INFO "smudrv: dma buff irq # %d\n", gpio_irqs[1]);

    return 0;
}

/**
 * @brief Destroy IRQs
 *
 * This function frees the requested IRQs 
 */
void dest_irq(void)
{
    free_irq(gpio_irqs[0],NULL);
    free_irq(gpio_irqs[1],NULL);
    gpio_free_array(signals, ARRAY_SIZE(signals));
}
