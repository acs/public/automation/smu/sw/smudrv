# **smuDRV** 

## OBS: OS Requirements

smuDRV works in 32-bit Raspberry Pi Operative Systems with kernel version < 6.4 (i.e. Bullseye and older).

## Linux driver for synchronized measurements

smuDRV is a kernel module that allows to interface the [**smuHAT**](smuHAT/README.md) hardware-on-top (HAT) for implementing a data acquisition system (DAQ) with time base synchronized to a highly accurate reference. Using a combination of parallel data exchange via secondary memory interface (SMI) and direct memory access (DMA) techniques, the driver optimizes the use of built-in peripherals to achieve high data throughput, low synchronization latency and CPU independent operation, providing high reliability for industrial applications without requiring a real-time (RT) environment.

## Architecture
The driver is composed of four sub-modules: the entry point **main** contains the calls to the kernel module entries _\_\_init_ and _\_\_exit_, which are used to initialize or free, respectively, the systems resources; **sub_dev** implements the character device interface; **sub_rpi** configures the hardware peripherals like GPIOs and IRQs; **sub_daq** configures the Raspberry Pi as a data acquisition board, defining sample rate, synchronization and operating modes.
```mermaid
flowchart LR
    main <--> sub_dev
    sub_dev <--> sub_rpi
    sub_dev <--> sub_daq
```

## Character device
The communication from the application in the user-space to the driver happens via IOCTL system calls, whereas system signals are used in the opposite direction. After opening the character device **/dev/smu**, the file descriptor **fd** handle can be used to perform all the essential file operations: _open()_, _release()_, _read()_, _write()_, _mmap()_, _ioctl()_. The first two, _open()_, _release()_,respectively lock and unlock a mutex to avoid concurrent access to the the resource. The following two, _read()_, _write()_, are legacy methods for reading the acquired data and configuring the DAQ parameters. They have been replaced by _mmap()_ and _ioctl()_.

### - _read()_
Legacy method for reading the data acquired from the ADC and stored in the internal buffer. This method is invoked by passing as arguments a pointer to a user memory allocation, the offset and length of data to be copied from the kernel buffer into the user buffer.
```mermaid
gantt
    axisFormat .
    section Buffer
    offset :done, off, 0, 5s
    data   :crit, 2s
```

### - _write()_
Legacy method for configuring the DAQ system. This methos is invoked by passing as argument a 4 bytes configuration structure that is copied from the user to the kernel space. The **smu\_conf\_t** structure contains four parameters: the sampling frequency **fs** in kilo samples per second (kSPS); the DMA buffer frame rate **fb**; the acquisition **mode** and the synchronization source **sync**.
```c
typedef struct
{
    uint8_t fs;                                                     // Sample rate (kHz)
    uint8_t fb;                                                     // Buffer rate (Hz)
    uint8_t mode;                                                   // Acquisition mode
    uint8_t sync;                                                   // Synchronization
} __attribute__((__packed__)) smu_conf_t;
```

### - _mmap()_
Creates a mapping of the kernel data buffer in the virtual address space of the calling process. This method is invoked by passing as argument a pointer to a user memory allocation, which is then linked to the internal buffer. This method is equivalent to _read()_, with the additionla advantage of providing full access to the entire raw data buffer, without requiring continuous file operation.

### - _ioctl()_
System call to manipulate the operating characteristics of the character device by sending control codes with attached payloads if required. This method is invoked for several control operations: **REG_TASK** is used to register the user-space process that wants to be able to receive signals from the kernel-space; **RESET** allows to perform a full reset of the driver; **START** and **STOP** control the status of the DAQ system; **SET/GET_CONF** with smu_conf_t payload superseed the _write()_ method, with the additional advantage of allowing to read the current configuration.
```mermaid
sequenceDiagram
  participant GPS
  participant DAQ
  participant Kernel
  participant User
  User->>Kernel: SMU_IOC_REG_TASK
  activate User
  User->>Kernel: SMU_IOC_START
  Kernel->>DAQ: Start DAQ
  activate DAQ
  DAQ->>DAQ: Data acquisition
  DAQ--)Kernel: Buffer IRQ
  Kernel--)User: Signal SMU_SIG_DATA
  GPS--)Kernel: PPS IRQ
  Kernel--)User: Signal SMU_SIG_SYNC
  User->>Kernel: SMU_IOC_STOP
  Kernel->>DAQ: Stop DAQ
  deactivate DAQ
  deactivate User
```

## DAQ finite state machine
The data acquisition system uses a sequence of seven DMA control blocks to create a finite state machine (FSM) that is indipendent from the activity of the operating system.
The DAQ starts with the ioctl command `SMU_IOC_START`, which instructs the DMA controller to load the first control block.
This configures the length register SMI_L of the secondary memory interface (SMI) for a transfer of length one.
The following block configures the SMI control status register SMI_CS to start and moves to the third block, which enables the PWM_CTL controller register to generate the sampling pulses.
If the DAQ is configured with `SYNC_PPS`, the FSM waits for an interrupt from the GPS before writing the PWM_CTL register.
Conversely, for `SYNC_NONE`, the DMA enables the PWM and moves directly to the fourth control block.
This instructs the DMA to configure the SMI_L register for a transfer of length N, equal to the number of samples in a second.
The fifth control block starts the SMI and moves to the sixth control block, which takes care of transfering N samples from the secondary memory interface FIFO buffer SMI_D to the driver virtual memory.
If the DAQ is configured for `SYNC_NONE` with `MODE_FREERUN`, the sixth block heads back to the fourth block creating a loop that can be interrupted by issueing the ioctl command `SMU_IOC_STOP`.
Otherwise, the DMA moves on to the last block that disables the PWM.
In case the DAQ is configured for `MODE_ONESHOT`, the DMA stops and the FSM is terminated.
Conversely, if the DAQ is configured for `MODE_FREERUN`, the DMA heads back to the first control block to start a new cycle in a loop that can be interrupted by issueing the ioctl command `SMU_IOC_STOP`.

```mermaid
flowchart LR
  start(( )) -->|Length 1| smi_l1(SMI_L)
  smi_l1 -->|Start| smi_cs1(SMI_CS)
  smi_cs1 -->|Enable| pwm_ctl
  pwm_ctl -->|Length N| smi_ln(SMI_L)
  gps([GPS]) -.->|SYNC_PPS ?| pwm_ctl(PWM_CTL)
  smi_ln -->|Start| smi_cs2(SMI_CS)
  smi_cs2 -->|Read data| smi_d(SMI_D)
  smi_d -->|Disable| pwm_2(PWM_CTL)
  adc([ADC]) -.->|Data irq| smi_d(SMI_D)
  pwm_2 -->|MODE_ONESHOT ?| stop(( ))
  pwm_2 -->|MODE_FREERUN ?| smi_l1
  smi_d -->|SYNC_NONE ?| smi_ln
```

## Download
Create the folder **smuDRV** inside the project folder **smu**, then clone the repository inside.
```
mkdir -p ~/smu
cd ~/smu
mkdir smuDRV
git clone <repoDRV> smuDRV
```

Repeat the same procedure for the common library component **smuLIB** (_ignore if already done_).
```
cd ~/smu
mkdir smuLIB
git clone <repoLIB> smuLIB
```

## Installation
To build the driver, we need first to download the Raspberry Pi Kernel Headers.
```
apt-get install raspberrypi-kernel-headers
```

Verify that the version of the installed headers corresponds with the kernel version in use. 
```
ls -l /lib/modules/$(uname -r)/build
uname -a
```

## Build
Open the folder and build the kernel module.
```
cd ~/smu/smuDRV
make
```

Upon succesful compilation, the built kernel module will be available in the project folder **build** with name **smudrv.ko**.
```
cd ~/smu/build
ls -l
```

## Usage
To be able to use the built kernel module it needs first to be enabled.
This can be done manually or automatically at boot.

### Manual
Insert the module in the kernel.
```
cd ~/smu/build
insmod smudrv.ko
```

Check the list of currently active modules and print the module's info.
```
lsmod | grep smudrv
modinfo smudrv
```

Display the kernel ring buffer to verify that the module was correctly loaded (_use argument -w for live log_).
```
dmesg
```

If required and not currently in use, it is possible to remove the driver
```
rmmod smudrv
```

### Automatic
Create a folder for the SMU driver in the system path and copy the kernel module, this will place the module in modprobe's database.
```
mkdir /lib/modules/`uname -r`/kernel/drivers/iio/smu
cp ~/smu/build/smudrv.ko /lib/modules/`uname -r`/kernel/drivers/iio/smu
```

Edit the modules configuration file and add `smudrv` in the first empty line.
```
sudo nano /etc/modules-load.d/modules.conf
```

Find all the dependencies of the new kernel module and reboot.
```
depmod
reboot
```

Check the list of currently active modules and print the module's info.
```
lsmod | grep smudrv
modinfo smudrv
```

Display the kernel ring buffer to verify that the module was correctly loaded (_use argument -w for live log_).
```
dmesg
```

## Copyright
2017-2021, Carlo Guarnieri (ACS) <br/>
2019-2021, César Andrés Cazal (ACS) <br/>
2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="LICENSE.md"><img alt="GNU General Public License v2.0" style="border-width:0" src="docs/gnu_gpl-2_logo.png" width="88"/></a><br />This work is licensed under the <a rel="license" href="LICENSE.md">GNU General Public License v2.0</a>.

## Funding
<a rel="funding" href="https://hyperride.eu/"><img alt="HYPERRIDE" style="border-width:0" src="docs/hyperride_logo.png" height="63"/></a>&nbsp;
<a rel="funding" href="https://cordis.europa.eu/project/id/957788"><img alt="H2020" style="border-width:0" src="https://hyperride.eu/wp-content/uploads/2020/10/europa_flag_low.jpg" height="63"/></a><br />
This work was supported by <a rel="Hyperride" href="https://hyperride.eu/">HYbrid Provision of Energy based on Reliability and Resiliency via Integration of DC Equipment</a> (HYPERRIDE), a project funded by the European Union's Horizon 2020 research and innovation programme under <a rel="H2020" href="https://cordis.europa.eu/project/id/957788"> grant agreement No. 957788</a>.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Carlo Guarnieri Calò Carducci, Ph.D.](mailto:carlo.guarnieri@ieee.org)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
